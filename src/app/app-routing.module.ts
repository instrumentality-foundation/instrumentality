import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { LandingGuard } from './core/guards/landing.guard';
import { DeactivatorGuard } from './core/guards/deactivator.guard';
import { DeveloperGuard } from './core/guards/developer.guard';
import { OrchestratorGuard } from './core/guards/orchestrator.guard';
import { CompanyGuard } from "./core/guards/company.guard";
import { StaticGuard } from './core/guards/static.guard';

const routes: Routes = [
  {
    path: 'landing',
    loadChildren: () => import('./landing/landing.module').then(m => m.LandingModule),
    canActivate: [LandingGuard]
  },
  {
    path: 'orchestrator',
    loadChildren: () => import('./orchestrator-dashboard/orchestrator-dashboard.module').then(m => m.OrchestratorDashboardModule),
    canActivate: [OrchestratorGuard],
    canDeactivate: [DeactivatorGuard]
  },
  {
    path: 'account/:account',
    loadChildren: () => import('./developer-dashboard/developer-dashboard.module').then(m => m.DeveloperDashboardModule),
    canActivate: [DeveloperGuard],
    canDeactivate: [DeactivatorGuard]
  },
  {
    path: 'company',
    loadChildren: () => import('./company-dashboard/company-dashboard.module').then(m => m.CompanyDashboardModule),
    canActivate: [CompanyGuard],
    canDeactivate: [DeactivatorGuard]
  },
  {
    path: 'static/:account',
    loadChildren: () => import('./static/static.module').then(m => m.StaticModule),
    canActivate: [StaticGuard]
  },
  {path: '', redirectTo: 'landing', pathMatch: 'full'}
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
