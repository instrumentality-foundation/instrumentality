import { Component, OnInit, AfterViewInit, ViewChild, ElementRef } from '@angular/core';
import * as Quill from 'quill';
import { faCode } from "@fortawesome/free-solid-svg-icons";
import { NotifierService, NotificationStatus } from 'src/app/core/services/notifier.service';
import { CompanyManager } from 'src/app/core/utils/company-manager';
import { SpinnerCombo } from 'src/app/core/services/spinner.service';

@Component({
  selector: 'company-command-panel',
  templateUrl: './command-panel.component.html',
  styleUrls: ['./command-panel.component.less']
})
export class CommandPanelComponent implements OnInit {

  // Job creation input fields
  public job_name: string;
  public job_studies: string;
  public job_city: string;
  public job_types = [
    {value: "Full time"}, {value: "Part time"},
    {value: "Internship"}, {value: "Project only"}
  ]
  public job_salary: number;
  private editor: any;
  private skills = [];
  private blockchain_skills = [];
  public private_key: string;
  
  // Elemnets from interface
  @ViewChild('skill_list', {static: true}) public skill_list: ElementRef;
  @ViewChild('job_type_edit', {static: true}) public job_type_edit: ElementRef;
  @ViewChild('publish_button', {static: true}) public publish_button: ElementRef;
  @ViewChild('publish_spinner', {static: true}) public publish_spinner: ElementRef;

  // Icon definitions
  public faCode = faCode;

  // Credentials
  private account_id = JSON.parse(localStorage.getItem('account')).id;
  private token = JSON.parse(localStorage.getItem('account')).token;

  // Task elements
  public new_task_title: string;
  public new_task_quill_editor: Quill;
  public employees = [];
  public new_task_skill_list = [];
  @ViewChild('new_task_spinner', {static: true}) public new_task_spinner: ElementRef;
  @ViewChild('create_task_button', {static: true}) public create_task_button: ElementRef;
  @ViewChild('new_task_difficulty', {static: true}) public new_task_difficulty: ElementRef;
  @ViewChild('new_task_asignee', {static: true}) public new_task_asignee: ElementRef;
  @ViewChild('new_task_skills', {static: true}) public new_task_skills: ElementRef;

  constructor(
    private notifier: NotifierService,
    private companyManager: CompanyManager
  ) { }

  ngOnInit(): void {
    this.companyManager.getAssetList()
    .then(response => {
      this.blockchain_skills = response.body['assets'];
    })
    .catch(reason => {
      console.log(reason);
    });

    // Load employees
    this.companyManager.loadEmployees(this.account_id, this.token)
    .then(response => {
      this.employees = response.body['employees'];
    })
    .catch(reason => {
      console.error(reason.error.msg);
    });
  }

  ngAfterViewInit(): void{
    // Initializing quill
    this.editor = new Quill('#quill_editor', {
      placeholder: "Describe the job in a couple of words",
      theme: 'snow'
    });

    this.new_task_quill_editor = new Quill('#new_task_quill_editor', {
      placeholder: "Task details",
      theme: 'snow'
    })
  }

  public addSkill() {
    if (this.skills.length == 10) {
      this.notifier.notifyUIkit('Maximum number of skills is 10!', NotificationStatus.warning);
      return;
    }

    // Create interface containers
    let skill_container = document.createElement('div');
    skill_container.classList.add('uk-width-1-2@m');
    let skill_level_container = document.createElement('div');
    skill_level_container.classList.add('uk-width-1-2@m');

    // Create interface input elements
    let skill_input = document.createElement('select');
    skill_input.classList.add('uk-select');
    skill_input.style.fontFamily = "'Lato', sans-serif";
    skill_input.style.fontSize = '1.2em';
    skill_input.style.color = '#F7BFBE';
    skill_input.style.backgroundColor = '#1B2021';
    skill_input.style.borderRadius = ".2em";
    skill_input.style.borderWidth = "2px";
    skill_input.style.borderColor = "#EF9FA4";

    let skill_default_option = document.createElement('option');
    skill_default_option.value = '';
    skill_default_option.hidden = true;
    skill_default_option.selected = true;
    skill_default_option.disabled = true;
    skill_default_option.text = "Select a skill";

    skill_input.options.add(skill_default_option)

    // Add options from blockchain
    this.blockchain_skills.forEach(skill => {
      let option = document.createElement('option');
      option.value = skill;
      option.textContent = skill;
      option.style.color = "#F7BFBE";
      option.style.backgroundColor = "#1B2021";

      skill_input.options.add(option);
    })

    let skill_level = document.createElement('input');
    skill_level.classList.add('uk-input');
    skill_level.type = 'number';
    skill_level.placeholder='Level of skill';
    skill_level.style.fontFamily = "'Lato', sans-serif";
    skill_level.style.fontSize = '1.2em';
    skill_level.style.color = '#F7BFBE';
    skill_level.style.backgroundColor = '#1B2021';
    skill_level.style.borderRadius = ".2em";
    skill_level.style.borderWidth = "2px";
    skill_level.style.borderColor = "#EF9FA4";

    // Add input elements to the containers and the containers to the form
    skill_container.appendChild(skill_input);
    skill_level_container.appendChild(skill_level);

    this.skill_list.nativeElement.appendChild(skill_container);
    this.skill_list.nativeElement.appendChild(skill_level_container);

    // Save the inputs for later retrieval
    let skill = {
      skill_name: skill_input,
      skill_level: skill_level
    }
    this.skills.push(skill);

  }

  private validateJobDetails() {
    let text = this.editor.getText();
    if (this.job_name == null || this.job_name == '') {
      this.notifier.notifyUIkit('Please fill in the job name', NotificationStatus.danger);
      return false;
    }
    if (text == '') {
      this.notifier.notifyUIkit('Please fill in the job description', NotificationStatus.danger);
      return false;
    }
    if (this.job_studies == null || this.job_studies == '') {
      this.notifier.notifyUIkit('Please fill in the required level of studies for this job', NotificationStatus.danger);
      return false;
    }
    if (this.job_city == null || this.job_city == '') {
      this.notifier.notifyUIkit('Please fill in the city where this job is available', NotificationStatus.danger);
      return false;
    }
    if (this.job_type_edit.nativeElement.selectedIndex == 0) {
      this.notifier.notifyUIkit('Please fill in the job type', NotificationStatus.danger);
      return false;
    }
    if (this.job_salary == null || this.job_salary <= 0) {
      this.notifier.notifyUIkit("Invalid value for job salary", NotificationStatus.danger);
      return false;
    }
    if (this.private_key == null || this.private_key == '') {
      this.notifier.notifyUIkit("Your private key is necessary to perform this operation", NotificationStatus.danger);
      return false;
    }

    return true;
  }

  /**
   * Publishes new job on the blockchain.
   * 
   * Mechanism:
   * ----------
   * 
   * 1. Form data is loaded in JSON.
   * 2. JSON is sent to Python server.
   * 3. Python parses the input and generates a JOB token for the company.
   * 4. The company transfers the token to an orchestrator.
   * 5. The orchestrator lists the JOB on the market, letting developers apply for it.
   * 6. After the company chooses a developer, the token is transfered to the developer
   * and the developer account is transfered in the company domain, thus binding the two.
   */
  public async publishJob() {
    // Frontend validation of input
    let delta = this.editor.getContents();
    if (!this.validateJobDetails()) {
      return;
    }

    // Get all the added skills and levels
    let skills = []
    let skill_levels = []

    this.skills.forEach(skill => {
      if (skill.skill_name.value != '' && skill.skill_name.value != null) {
        skills.push(skill.skill_name.value);

        if (skill.skill_level.value != null && skill.skill_level.value != 0) {
          skill_levels.push(skill.skill_level.value);
        }
        else {
          skill_levels.push(0);
        }
      }
      
    });

    // Load JSON
    let job = {
      name: this.job_name,
      description: JSON.stringify(delta),
      study: this.job_studies,
      city: this.job_city,
      type: this.job_type_edit.nativeElement.value,
      salary: this.job_salary,
      skills: skills,
      skill_levels: skill_levels
    }

    // Create spinner combo
    let spinnerCombo = new SpinnerCombo(this.publish_button, this.publish_spinner);
    spinnerCombo.toggle();

    try {
      let response = await this.companyManager.publishJob(this.account_id, this.private_key, JSON.stringify(job));
      console.log(response.body);
      this.notifier.notifyUIkit(response.body['msg'], NotificationStatus.sucess);
    }
    catch(reason) {
      console.error(reason.error);
      this.notifier.notifyUIkit(reason.error.msg, NotificationStatus.danger);
    }
    finally {
      spinnerCombo.toggle();
    }
  }

  public async addNewTaskSkill() {
    if (this.new_task_skill_list.length == 3) {
      this.notifier.notifyUIkit('Maximum number of skills is 3!', NotificationStatus.warning);
      return;
    }

    // Create interface containers
    let skill_container = document.createElement('div');
    skill_container.classList.add('uk-width-1-3@m');
    skill_container.classList.add('uk-padding-remove');
    skill_container.classList.add('uk-flex');
    
     // Create interface input elements
     let skill_input = document.createElement('select');
     skill_input.classList.add('uk-select');
     skill_input.style.fontFamily = "'Lato', sans-serif";
     skill_input.style.fontSize = '1.2em';
     skill_input.style.color = '#F7BFBE';
     skill_input.style.backgroundColor = '#1B2021';
     skill_input.style.borderRadius = ".2em";
     skill_input.style.borderWidth = "2px";
     skill_input.style.borderColor = "#EF9FA4";
     skill_input.style.width = '50%';
     skill_input.style.marginLeft = 'auto';
     skill_input.style.marginRight = 'auto';
 
     let skill_default_option = document.createElement('option');
     skill_default_option.value = '';
     skill_default_option.hidden = true;
     skill_default_option.selected = true;
     skill_default_option.disabled = true;
     skill_default_option.text = "Select a skill";
 
     skill_input.options.add(skill_default_option)
 
     // Add options from blockchain
     this.blockchain_skills.forEach(skill => {
       let option = document.createElement('option');
       option.value = skill;
       option.textContent = skill;
       option.style.color = "#F7BFBE";
       option.style.backgroundColor = "#1B2021";
 
       skill_input.options.add(option);
     })
 
     // Add input elements to the containers and the containers to the form
     skill_container.appendChild(skill_input);
 
     this.new_task_skills.nativeElement.appendChild(skill_container);
 
     // Save the inputs for later retrieval
     let skill = {
       skill_name: skill_input,
     }
     this.new_task_skill_list.push(skill);
  }

  private validateTask() {
    if (this.new_task_title == '') {
      this.notifier.notifyUIkit("Please fill in the task name!", NotificationStatus.danger);
      return false;
    }
    if (this.new_task_asignee.nativeElement.selectedIndex == 0) {
      this.notifier.notifyUIkit("Please asign this task to one of your developers!", NotificationStatus.danger)
      return false;
    }
    if (this.new_task_difficulty.nativeElement.selectedIndex == 0) {
      this.notifier.notifyUIkit("Please select a difficulty for this task!", NotificationStatus.danger)
      return false;
    }
    if (this.private_key == '') {
      this.notifier.notifyUIkit("Please fill in the private key field!", NotificationStatus.danger)
      return false;
    }

    return true;
  }

  public async createTask() {
    let delta = this.new_task_quill_editor.getContents();

    if (!this.validateTask()) {
      return;
    }

    let skill_list = []
    this.new_task_skill_list.forEach(skill => {
      if (skill.skill_name.value != '' && skill.skill_name.value != null) {
        skill_list.push(skill.skill_name.value);
      }
    });

    let task = {
      name: this.new_task_title,
      difficulty: this.new_task_difficulty.nativeElement.value,
      description: JSON.stringify(delta),
      skills: skill_list
    };

    let spinnerCombo = new SpinnerCombo(this.create_task_button, this.new_task_spinner);
    spinnerCombo.toggle();
    try {
      let response = await this.companyManager.createTask(this.account_id, this.token, this.private_key, this.new_task_asignee.nativeElement.value, task);
      console.log(response);
      this.notifier.notifyUIkit(response.body['msg'], NotificationStatus.sucess);
    }
    catch (reason) {
      console.error(reason.error);
      this.notifier.notifyUIkit(reason.error.msg, NotificationStatus.danger);
    }
    finally {
      spinnerCombo.toggle();
    }
  }

}
