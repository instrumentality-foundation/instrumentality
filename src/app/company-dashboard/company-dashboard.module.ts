import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from "@angular/forms";
import { HttpClientModule } from "@angular/common/http";
import { CoreModule } from "../core/core.module";
import { FontAwesomeModule } from "@fortawesome/angular-fontawesome";

import { CompanyDashboardRoutingModule } from './company-dashboard-routing.module';
import { CompanyDashboardComponent } from './company-dashboard.component';
import { NavbarComponent } from './navbar/navbar.component';
import { ProfileComponent } from './profile/profile.component';
import { CommandPanelComponent } from './command-panel/command-panel.component';
import { JobsComponent } from './jobs/jobs.component';


@NgModule({
  declarations: [CompanyDashboardComponent, NavbarComponent, ProfileComponent, CommandPanelComponent, JobsComponent],
  imports: [
    CommonModule,
    CompanyDashboardRoutingModule,
    FormsModule,
    HttpClientModule,
    CoreModule,
    FontAwesomeModule
  ]
})
export class CompanyDashboardModule { }
