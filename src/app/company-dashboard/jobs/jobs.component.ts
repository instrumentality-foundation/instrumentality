import { Component, OnInit, AfterViewInit } from '@angular/core';
import { CompanyManager } from 'src/app/core/utils/company-manager';

@Component({
  selector: 'company-jobs',
  templateUrl: './jobs.component.html',
  styleUrls: ['./jobs.component.less']
})
export class JobsComponent implements OnInit {

  public jobs = [];

  // Credentials
  public account_id: string = JSON.parse(localStorage.getItem('account')).id;
  private token = JSON.parse(localStorage.getItem('account')).token;

  constructor(
    private companyManager: CompanyManager
  ) { }

  ngOnInit(): void {
  }

  ngAfterViewInit(): void {
    this.companyManager.getCompanyJobs(this.account_id)
    .then(response => {
      this.jobs = response.body['jobs'];
      console.log(this.jobs);
    })
    .catch(reason => {
      console.error(reason.error);
    });
  }

}
