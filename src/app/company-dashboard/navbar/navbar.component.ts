import { Component, OnInit } from '@angular/core';
import { faSignOutAlt, faBars } from "@fortawesome/free-solid-svg-icons";

@Component({
  selector: 'company-navbar',
  templateUrl: './navbar.component.html',
  styleUrls: [
    './navbar.component.less',
    '../../core/styles/navbar.less'
  ]
})
export class NavbarComponent implements OnInit {

  // Icons
  public faSignOutAlt = faSignOutAlt;
  public faBars = faBars;

  constructor() { }

  ngOnInit(): void {
  }

}
