import { Component, OnInit, AfterViewInit, ViewChild, ElementRef } from '@angular/core';
import { faCameraRetro, faPencilAlt } from "@fortawesome/free-solid-svg-icons";
import { NotifierService, NotificationStatus } from 'src/app/core/services/notifier.service';
import { AccountManager } from 'src/app/core/utils/account-manager';
import { SpinnerCombo } from "src/app/core/services/spinner.service";
import { CompanyManager } from 'src/app/core/utils/company-manager';

@Component({
  selector: 'company-profile',
  templateUrl: './profile.component.html',
  styleUrls: [
    './profile.component.less',
    '../../core/styles/profile-section.less'
  ]
})
export class ProfileComponent implements OnInit {

  // Input items
  public edit_company_name: any;

  // Icon definitions
  public faCameraRetro = faCameraRetro;
  public faPencilAlt = faPencilAlt;

  // Account credentials
  private account = JSON.parse(localStorage.getItem('account'));
  public account_id = this.account.id;
  private token = this.account.token;

  // Modal elements
  @ViewChild('modal_profile', {static: true}) modalProfile: ElementRef;
  @ViewChild('profile_image', {static: true}) profileImage: ElementRef;

  // Spinner parts
  @ViewChild('save_button', {static: true}) saveButton: ElementRef;
  @ViewChild('save_spinner', {static: true}) saveSpinner: ElementRef;

  constructor(
    private notifier: NotifierService,
    private accountManager: AccountManager,
    private companyManager: CompanyManager
  ) { }

  ngOnInit(): void {
    // Trying to load info from local storage
    let info = JSON.parse(localStorage.getItem(this.account_id));

    if (info) {
      this.edit_company_name = info.company_name;
    }
    else {
      // Load info from blockchain if it exists
      this.companyManager.getCompanyInfo(this.account_id, this.token)
      .then(response => {
        console.log(response.body);
        info = response.body['info'];
        localStorage.setItem(this.account_id, JSON.stringify(info));
        this.edit_company_name = info.company_name;
      })
      .catch(reason => {
        console.error(reason.error);
      });
    }
  }

  ngAfterViewInit(): void {
    // Load the profile image
    let request = this.accountManager.loadMyAccountImage(this.account_id, this.token);
    request.then(response => {
      console.log(response.body);
      this.modalProfile.nativeElement.src = URL.createObjectURL(response.body);
      this.profileImage.nativeElement.src = URL.createObjectURL(response.body);
    })
    .catch(reason => {
      console.error(reason.error);
    })
  }

  public async setInfo() {
    let company_name = this.edit_company_name != null ? this.edit_company_name : '';
    let info = {
      company_name: company_name
    }

    // Create a spinner combo
    let spinnerCombo = new SpinnerCombo(this.saveButton, this.saveSpinner)
    spinnerCombo.toggle();

    // Making a request to the server
    try {
      let response = await this.companyManager.setCompanyInfo(this.account_id, this.token, info);
      console.log(response.body['msg']);
      this.notifier.notifyUIkit(response.body['msg'], NotificationStatus.sucess);
      localStorage.setItem(this.account_id,
        JSON.stringify(info));
    }
    catch(reason) {
      console.error(reason.error);
      this.notifier.notifyUIkit(reason.error.msg, NotificationStatus.danger);
    }
    finally{
      spinnerCombo.toggle();
    }
  }

  public async changeProfilePicture() {
    // Create file input on-the-fly
    let input = document.createElement('input');
    input.type = 'file';
    input.accept = "image/svg, image/png, image/jpg, image/jpeg"
    input.click();

    let form_data = new FormData();

    let file : File;
    
    // At input onchange event we make the server request
    input.onchange = async event => {
      let target = event.target as any;
      file = target.files[0];

      form_data.append('image', file);
      form_data.append('username', this.account_id);
      form_data.append('token', this.token);

      try {
        let response = await this.accountManager.setMyAccountImage(form_data);
        console.log(response.body);
        this.notifier.notifyUIkit('Profile picture set successfully', NotificationStatus.sucess);
      
        // We now load the picture
        try {
          let response2 = await this.accountManager.loadMyAccountImage(this.account_id, this.token);
          console.log(response2.body);
          this.modalProfile.nativeElement.src = URL.createObjectURL(response2.body);
          this.profileImage.nativeElement.src = URL.createObjectURL(response2.body);
        }
        catch (reason) {
          console.error(reason.error);
        }
      }
      catch(reason) {
        console.error(reason.error);
        this.notifier.notifyUIkit(reason.error, NotificationStatus.danger);
      }
    }
  }

}
