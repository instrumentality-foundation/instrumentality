import { Component, OnInit, ViewChild, ElementRef, Input } from '@angular/core';

@Component({
  selector: 'core-card',
  templateUrl: './card.component.html',
  styleUrls: ['./card.component.less']
})
export class CardComponent implements OnInit {

  @Input() public card_image: string;
  @Input() public card_image_description: string;
  @Input() public card_button: string;
  @Input() public card_title: string;
  @Input() public card_detail: string;

  // Card faces
  @ViewChild('front_card', {static: true}) frontCard: ElementRef;
  @ViewChild('back_card', {static: true}) backCard: ElementRef;

  constructor() { }

  ngOnInit(): void {
  }

  public flipCard() {
    if (this.frontCard.nativeElement.style.display != "none") {
      this.frontCard.nativeElement.style.display = "none";
      this.backCard.nativeElement.style.display = "block";
      this.frontCard.nativeElement.classList.add('uk-animation-scale-down');
    }
    else {
      this.frontCard.nativeElement.style.display = "block";
      this.backCard.nativeElement.style.display = "none";
    }
  }

}
