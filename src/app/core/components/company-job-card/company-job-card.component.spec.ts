import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CompanyJobCardComponent } from './company-job-card.component';

describe('CompanyJobCardComponent', () => {
  let component: CompanyJobCardComponent;
  let fixture: ComponentFixture<CompanyJobCardComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CompanyJobCardComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CompanyJobCardComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
