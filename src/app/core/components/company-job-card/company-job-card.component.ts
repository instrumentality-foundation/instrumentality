import { Component, OnInit, Input, ViewChild, ElementRef } from '@angular/core';
import { faMapMarkerAlt, faBuilding, faUniversity, faBusinessTime, faMoneyBill, faTrash, faUserAlt } from "@fortawesome/free-solid-svg-icons";
import { CompanyManager } from "../../utils/company-manager";
import { AccountManager } from "../../utils/account-manager";
import { DevManager } from "../../utils/dev-manager";
import { NotifierService, NotificationStatus } from "../../services/notifier.service";
import * as randomcolor from 'randomcolor';
import * as Quill from 'quill'
import { SpinnerCombo } from '../../services/spinner.service';

@Component({
  selector: 'core-company-job-card',
  templateUrl: './company-job-card.component.html',
  styleUrls: ['./company-job-card.component.less']
})
export class CompanyJobCardComponent implements OnInit {

  @Input() public job: any;
  @Input() public card_modal_id: string;
  public applicants_modal_id: string;
  public editor: any;
  public job_text: string;
  public domain: string;
  public company_name: string;
  public colors = [];
  public applicants = [];

  // DOM elements
  @ViewChild('company_logo', {static: true}) public company_logo: ElementRef;
  @ViewChild('delete_button', {static: true}) public delete_button: ElementRef;
  @ViewChild('delete_spinner', {static: true}) public delete_spinner: ElementRef;

  // Crdentials
  public account_id = JSON.parse(localStorage.getItem('account')).id;
  private token = JSON.parse(localStorage.getItem('account')).token;

  // Icons
  public faMapMarkerAlt = faMapMarkerAlt;
  public faBuilding = faBuilding;
  public faUniversity = faUniversity;
  public faBusinessTime = faBusinessTime;
  public faMoneyBill = faMoneyBill;
  public faTrash = faTrash;
  public faUserAlt = faUserAlt;

  constructor(
    private companyManager: CompanyManager,
    private accountManager: AccountManager,
    private devManager: DevManager,
    private notifier: NotifierService
  ) {}

  ngOnInit(): void {
    this.domain = this.job.company.split('@')[1];
    this.applicants_modal_id = this.card_modal_id + '_applicants';

    // Loading image
    this.companyManager.getCompanyLogo(this.account_id, this.token, this.job.company)
    .then(response => {
      this.company_logo.nativeElement.src = URL.createObjectURL(response.body);
    })
    .catch(reason => {
      console.error(reason.error);
    })

    // Load company details
    this.companyManager.getCompanyPublicInfo(this.account_id, this.token, this.job.company)
    .then(response => {
      console.log(response.body);
      this.company_name = response.body['info'].company_name;
    })
    .catch(reason => {
      console.error(reason.error);
    });

    // Load applicants
    this.companyManager.loadApplicants(this.account_id, this.token, this.job._id)
    .then(response => {
      this.applicants = response.body['applicants'];
    })
    .catch(reason => {
      console.error(reason.error);
    });

    // Load colors
    this.colors = randomcolor.randomColor({
      luminosity: 'light',
      alphy: 0.5,
      hue: 'random',
      count: this.job.skills.length
    });
  }

  ngAfterViewInit(): void {
    this.editor = new Quill('#quill_editor_'+this.card_modal_id,
    {
      modules: {
        toolbar: false
      },
      theme: 'snow',
      readOnly: true
    });

    this.editor.setContents(JSON.parse(this.job.description));
    this.job_text = this.editor.getText();
  }

  public async deleteJob() {
    let spinnerCombo = new SpinnerCombo(this.delete_button, this.delete_spinner);
    spinnerCombo.toggle();

    try {
      let response = await this.companyManager.deleteJob(this.account_id, this.token, this.job._id);
      console.log(response.body);
      this.notifier.notifyUIkit(response.body['msg'], NotificationStatus.sucess);
    }
    catch (reason) {
      console.error(reason.error);
      this.notifier.notifyUIkit(reason.error.msg, NotificationStatus.danger)
    }
    finally {
      spinnerCombo.toggle();
      window.location.reload();
    }
  }
}
