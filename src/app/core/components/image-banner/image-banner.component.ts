import { Component, OnInit, Input } from '@angular/core';

@Component({
  selector: 'core-image-banner',
  templateUrl: './image-banner.component.html',
  styleUrls: ['./image-banner.component.less']
})
export class ImageBannerComponent implements OnInit {

  @Input() public banner_image: string;
  @Input() public banner_image_description: string;
  @Input() public banner_title: string;

  constructor() { }

  ngOnInit(): void {
  }

}
