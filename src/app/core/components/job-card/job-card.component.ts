import { Component, OnInit, AfterViewInit, Input, ViewChild, ElementRef } from '@angular/core';
import * as Quill from 'quill';
import * as randomcolor from 'randomcolor';
import { CompanyManager } from '../../utils/company-manager';
import { AccountManager } from '../../utils/account-manager';
import { NotifierService, NotificationStatus } from '../../services/notifier.service';
import { faMapMarkerAlt, faBuilding, faUniversity, faBusinessTime, faMoneyBill } from "@fortawesome/free-solid-svg-icons";
import { DevManager } from '../../utils/dev-manager';

@Component({
  selector: 'core-job-card',
  templateUrl: './job-card.component.html',
  styleUrls: ['./job-card.component.less']
})
export class JobCardComponent implements OnInit {

  @Input() public job: any;
  @Input() public card_modal_id: string;
  public editor: any;
  public job_text: string;
  public domain: string;
  public company_name: string;
  public colors = []

  // DOM elements
  @ViewChild('company_logo', {static: true}) public company_logo: ElementRef;

  // Crdentials
  public account_id = JSON.parse(localStorage.getItem('account')).id;
  private token = JSON.parse(localStorage.getItem('account')).token;

  // Icons
  public faMapMarkerAlt = faMapMarkerAlt;
  public faBuilding = faBuilding;
  public faUniversity = faUniversity;
  public faBusinessTime = faBusinessTime;
  public faMoneyBill = faMoneyBill;

  constructor(
    private companyManager: CompanyManager,
    private accountManager: AccountManager,
    private devManager: DevManager,
    private notifier: NotifierService
  ) {}

  ngOnInit(): void {
    this.domain = this.job.company.split('@')[1];

    // Loading image
    this.companyManager.getCompanyLogo(this.account_id, this.token, this.job.company)
    .then(response => {
      this.company_logo.nativeElement.src = URL.createObjectURL(response.body);
    })
    .catch(reason => {
      console.error(reason.error);
    })

    // Load company details
    this.companyManager.getCompanyPublicInfo(this.account_id, this.token, this.job.company)
    .then(response => {
      console.log(response.body);
      this.company_name = response.body['info'].company_name;
    })
    .catch(reason => {
      console.error(reason.error);
    });

    // Load colors
    this.colors = randomcolor.randomColor({
      luminosity: 'light',
      alphy: 0.5,
      hue: 'random',
      count: this.job.skills.length
    });
  }

  ngAfterViewInit(): void {
    this.editor = new Quill('#quill_editor_'+this.card_modal_id,
    {
      modules: {
        toolbar: false
      },
      theme: 'snow',
      readOnly: true
    });

    this.editor.setContents(JSON.parse(this.job.description));
    this.job_text = this.editor.getText();
  }

  public async applyForJob() {
    try {
      let response = await this.devManager.applyForJob(this.account_id, this.token, this.job.company, this.job._id);
      console.log(response.body);
      this.notifier.notifyUIkit(response.body['msg'], NotificationStatus.sucess);
    }
    catch(reason) {
      console.error(reason.error);
      this.notifier.notifyUIkit(reason.error.msg, NotificationStatus.danger);
    }
  }

}
