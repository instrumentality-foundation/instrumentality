import { Component, OnInit, Input } from '@angular/core';

@Component({
  selector: 'core-modal',
  templateUrl: './modal.component.html',
  styleUrls: ['./modal.component.less']
})
export class ModalComponent implements OnInit {

  @Input() public modal_id : string;
  @Input() public modal_title: string;

  constructor() { }

  ngOnInit(): void {
  }

}
