import { Component, OnInit, Input, ViewChild, ElementRef, AfterViewInit } from '@angular/core';
import * as Quill from 'quill';
import * as randomcolor from 'randomcolor';
import { faCheckDouble, faTasks } from "@fortawesome/free-solid-svg-icons";
import { SpinnerCombo } from '../../services/spinner.service';
import { DevManager } from '../../utils/dev-manager';
import { NotifierService, NotificationStatus } from '../../services/notifier.service';

@Component({
  selector: 'core-task',
  templateUrl: './task.component.html',
  styleUrls: ['./task.component.less']
})
export class TaskComponent implements OnInit {

  @Input() public task: any;
  @Input() public task_modal_id: string;
  public task_quill: Quill;
  public difficulty_color: string;
  public skill_colors = []

  public faCheckDouble = faCheckDouble;
  public faTasks = faTasks;

  @ViewChild('done_spinner', {static: true}) public done_spinner: ElementRef;
  @ViewChild('done_button', {static: true}) public done_button: ElementRef;

  account_id = JSON.parse(localStorage.getItem('account')).id;
  token = JSON.parse(localStorage.getItem('account')).token;

  constructor(
    private devManager: DevManager,
    private notifier: NotifierService
  ) { }

  ngOnInit(): void {
    switch (this.task.difficulty) {
      case 'Easy':
        this.difficulty_color = '#70B77E';
        break;
      case 'Medium':
        this.difficulty_color = '#e2c873';
        break;
      case 'Hard':
        this.difficulty_color = '#e2737b';
      default:
        break;
    }

    this.skill_colors = randomcolor.randomColor({luminosity: 'light', alpha: 0.8, count: this.task.skills.length})
  }

  ngAfterViewInit(): void {
    this.task_quill = new Quill(
      '#' + this.task_modal_id +  '-quill',
      {
        modules: {
          toolbar: false
        },
        readOnly: true,
        theme: 'snow'
      }
    );

    this.task_quill.setContents(JSON.parse(this.task.description));
  }

  public async markDone() {
    let spinnerCombo = new SpinnerCombo(this.done_button, this.done_spinner);
    spinnerCombo.toggle();

    try {
      let response = await this.devManager.cashRewards(this.account_id, this.token, this.task.id);
      console.log(response.body);
      this.notifier.notifyUIkit(response.body['msg'], NotificationStatus.sucess);
    }
    catch (reason) {
      console.error(reason.error);
      this.notifier.notifyUIkit(reason.error.msg, NotificationStatus.danger);
    }
    finally {
      spinnerCombo.toggle();
    }
  }

}
