import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { HttpClientModule } from "@angular/common/http";
import { FontAwesomeModule } from "@fortawesome/angular-fontawesome";
import { FormsModule } from "@angular/forms";

import { ModalComponent } from "./components/modal/modal.component";
import { AuthenticationService } from "./services/authentication.service";
import { NotifierService } from "./services/notifier.service";
import { SpinnerService } from "./services/spinner.service";
import { TokenDestroyerDirective } from './directives/token-destroyer.directive';
import { AccountManager } from "./utils/account-manager";
import { OrchestrationManager } from "./utils/orchestration-manager";
import { CompanyManager } from "./utils/company-manager";
import { DeactivatorGuard } from "./guards/deactivator.guard";
import { DeveloperGuard } from "./guards/developer.guard";
import { LandingGuard } from "./guards/landing.guard";
import { OrchestratorGuard } from "./guards/orchestrator.guard";
import { StaticGuard } from "./guards/static.guard";
import { CardComponent } from './components/card/card.component';
import { ImageBannerComponent } from './components/image-banner/image-banner.component';
import { JobCardComponent } from './components/job-card/job-card.component';
import { DevManager } from "./utils/dev-manager";
import { CompanyJobCardComponent } from './components/company-job-card/company-job-card.component';
import { RouterModule } from '@angular/router';
import { TaskComponent } from './components/task/task.component';

@NgModule({
  declarations: [
    ModalComponent,
    TokenDestroyerDirective,
    CardComponent,
    ImageBannerComponent,
    JobCardComponent,
    CompanyJobCardComponent,
    TaskComponent
  ],
  imports: [
    CommonModule,
    HttpClientModule,
    FontAwesomeModule,
    FormsModule,
    RouterModule
  ],
  exports: [
    ModalComponent,
    CardComponent,
    ImageBannerComponent,
    JobCardComponent,
    CompanyJobCardComponent,
    TaskComponent,
    TokenDestroyerDirective
  ],
  providers: [
    AuthenticationService,
    NotifierService,
    SpinnerService,
    AccountManager,
    OrchestrationManager,
    CompanyManager,
    DevManager,
    DeactivatorGuard,
    DeveloperGuard,
    LandingGuard,
    OrchestratorGuard,
    StaticGuard
  ]
})
export class CoreModule { }
