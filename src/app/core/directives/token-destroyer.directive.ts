import { Directive, ElementRef, HostListener } from '@angular/core';
import { Router } from '@angular/router';

@Directive({
  selector: '[coreTokenDestroyer]'
})
export class TokenDestroyerDirective {

  private element: ElementRef;

  constructor(
    el: ElementRef,
    private router: Router
  ) { 
    this.element = el.nativeElement;
  }

  @HostListener('click') public destroyToken() {
    try {
      let account = JSON.parse(localStorage.getItem('account'));
      if (account) {
        localStorage.removeItem(account.id);
      }
      localStorage.removeItem('account');
      this.router.navigateByUrl("/");
    }
    catch(exception) {
      console.log(exception);
    }
  }
}
