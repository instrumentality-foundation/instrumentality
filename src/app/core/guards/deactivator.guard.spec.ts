import { TestBed } from '@angular/core/testing';

import { DeactivatorGuard } from './deactivator.guard';

describe('DeactivatorGuard', () => {
  let guard: DeactivatorGuard;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    guard = TestBed.inject(DeactivatorGuard);
  });

  it('should be created', () => {
    expect(guard).toBeTruthy();
  });
});
