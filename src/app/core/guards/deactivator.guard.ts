import { Injectable } from '@angular/core';
import { CanDeactivate, ActivatedRouteSnapshot, RouterStateSnapshot, UrlTree } from '@angular/router';
import { Observable, of } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class DeactivatorGuard implements CanDeactivate<unknown> {
  canDeactivate(
    component: unknown,
    currentRoute: ActivatedRouteSnapshot,
    currentState: RouterStateSnapshot,
    nextState?: RouterStateSnapshot): Observable<boolean | UrlTree> | Promise<boolean | UrlTree> | boolean | UrlTree {
    
    let account = localStorage.getItem('account');

    let static_regex = /^\/static\/[a-z_0-9]{1,32}@[A-Za-z_0-9]{1,32}\/profile$/

    if (account) {

      if(static_regex.test(nextState.url)) {
        return true;
      }
      else {
        return false;
      }
    }
    else {
      return of(true);
    }

  }
  
}
