import { Injectable } from '@angular/core';
import { CanActivate, ActivatedRouteSnapshot, RouterStateSnapshot, UrlTree, Router } from '@angular/router';
import { Observable, of } from 'rxjs';
import { AccountManager, AccountType } from '../utils/account-manager';
import { HttpClient } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class DeveloperGuard implements CanActivate {

  constructor(
    private http: HttpClient,
    private router: Router
  ){}


  canActivate(
    next: ActivatedRouteSnapshot,
    state: RouterStateSnapshot): Observable<boolean | UrlTree> | Promise<boolean | UrlTree> | boolean | UrlTree {

    let accountManager = new AccountManager(this.http);

    let account = JSON.parse(localStorage.getItem('account'));

    if(account) {
      return accountManager.verifyToken(account.id, account.token)
        .then(response => {
          if(response.body['status'] == 'success') {
            return accountManager.getMyAccountType(account.id, account.token)
            .then(type => {
              if (type == AccountType.developer) {
                return true;
              }
              else
                return false;
            })
          }
        })
        .catch(reason => {
          console.error(reason.error);
          return this.router.parseUrl("/landing/home");
        });
    }
    else {
      return of(false);
    }

  }
  
}
