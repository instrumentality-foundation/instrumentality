import { Injectable } from '@angular/core';
import { CanActivate, ActivatedRouteSnapshot, RouterStateSnapshot, UrlTree, Router } from '@angular/router';
import { Observable, of } from 'rxjs';
import { AccountType, AccountManager } from '../utils/account-manager';
import { HttpClient } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class LandingGuard implements CanActivate {

  constructor(
    private http: HttpClient,
    private router: Router
  ){}

  canActivate(
    next: ActivatedRouteSnapshot,
    state: RouterStateSnapshot): Observable<boolean | UrlTree> | Promise<boolean | UrlTree> | boolean | UrlTree {
    
    let accountManager = new AccountManager(this.http)

    let account = JSON.parse(localStorage.getItem('account'));

    if(!account) {
      return of(true);
    }
    else {
      let request = accountManager.getMyAccountType(account.id, account.token);
      return request.then(type => {
        console.log(type);
        if(type == AccountType.orchestrator) {
          return this.router.parseUrl('/orchestrator/deck');
        }
        else if (type == AccountType.company) {
          return this.router.parseUrl('/company');
        }
        else {
          return this.router.parseUrl('/account/' + account.id);
        }
      });
    }

  }
  
}
