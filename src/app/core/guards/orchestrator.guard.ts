import { Injectable } from '@angular/core';
import { CanActivate, ActivatedRouteSnapshot, RouterStateSnapshot, UrlTree, Router } from '@angular/router';
import { Observable, of } from 'rxjs';
import { HttpClient } from '@angular/common/http';
import { AccountManager, AccountType } from '../utils/account-manager';

@Injectable({
  providedIn: 'root'
})
export class OrchestratorGuard implements CanActivate {

  constructor(
    private http: HttpClient,
    private router: Router
  ){}

  canActivate(
    next: ActivatedRouteSnapshot,
    state: RouterStateSnapshot): Observable<boolean | UrlTree> | Promise<boolean | UrlTree> | boolean | UrlTree {
    
    let accountManager = new AccountManager(this.http);

    let account = JSON.parse(localStorage.getItem('account'));

    if(!account) {
      return of(false);
    }
    else {
      let request = accountManager.getMyAccountType(account.id, account.token);
      return request.then(type => {
        if (type == AccountType.orchestrator) {
          return accountManager.verifyToken(account.id, account.token)
          .then(_ => {
            return true;
          })
          .catch(_ => {
            return this.router.parseUrl('/landing');
          })
        }
        else {
          return false;
        }
      })
    }

  }
  
}
