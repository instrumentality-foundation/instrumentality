import { TestBed } from '@angular/core/testing';

import { StaticGuard } from './static.guard';

describe('StaticGuard', () => {
  let guard: StaticGuard;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    guard = TestBed.inject(StaticGuard);
  });

  it('should be created', () => {
    expect(guard).toBeTruthy();
  });
});
