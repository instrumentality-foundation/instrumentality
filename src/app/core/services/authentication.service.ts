import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { NotifierService, NotificationStatus } from './notifier.service';
import { constants } from "../constants";

@Injectable({
  providedIn: 'root'
})
export class AuthenticationService {

  private account_pattern = /^[a-z_0-9]{1,32}$/;

  constructor(
    private http: HttpClient,
    private notifier: NotifierService
  ) { }

  public async registerAccount (accountId: string) {
    // Check accountId format for errors
    if (this.account_pattern.test(accountId) === false) {
      this.notifier.notifyUIkit("Use only small letters, numbers and _", NotificationStatus.danger);
      console.error("Wrong username format");
    }

    return this.http.get(
      constants.iroha_network + '/account/create/' + accountId,
      {
        responseType: 'json',
        observe: 'response'
      }
    ).toPromise();
  }

  public async authenticateAccount (accountId: string, privateKey: string) {
    return this.http.post(
      constants.iroha_network + '/account/auth',
      {
        username: accountId,
        privateKey: privateKey
      },
      {
        observe: 'response',
        responseType: 'json'
      }
    ).toPromise();
  }
}
