import { Injectable } from '@angular/core';
import * as UIkit from "uikit";

@Injectable({
  providedIn: 'root'
})
export class NotifierService {

  /**
   * This method creates and displays a notification that's styled according to
   * its status.
   * For more info:
   * @see https://getuikit.com/docs/notification
   * 
   * @param message The message to be displayed
   * @param status The status of the notification: danger, success, warning, etc.
   */
  public notifyUIkit(message: string, status: NotificationStatus) {
    UIkit.notification(message, status);
  }

  /**
   * This method can create modal alerts with any html content.
   * For more info:
   * @see https://getuikit.com/docs/modal
   * 
   * @param html Any html content
   */
  public createModalUIkit(html: string) {
    var modal = UIkit.modal.dialog(html, {
      "bg-close": false
    });

    modal.show();
  }

  constructor() { }
}

export enum NotificationStatus {
  sucess = 'success',
  danger = 'danger',
  warning = 'warning'
}