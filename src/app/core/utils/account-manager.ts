import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { constants } from '../constants';
import { NotifierService, NotificationStatus } from '../services/notifier.service';

@Injectable({providedIn: 'root'})
export class AccountManager {
    
    constructor(
        private http: HttpClient
    ) {}

    /**
     * This method makes a request to the Python server to retreive
     * all extra-data of a specified account. You need to pass a valid token
     * associated with that account for authorization.
     * 
     * @param account_id The name of the account to search
     * @param token The token associated with that account
     */
    public async loadMyAccountData(account_id: string, token: string) {
        return this.http.put(
            constants.iroha_network + '/account/get',
            {
                username: account_id,
                token: token
            },
            {
                responseType: 'json',
                observe: 'response'
            }
        ).toPromise()
    }

    /**
     * This method loads the profile image stored on the server at the user's request.
     * 
     * @param account_id The name of the accout to get imafe for
     * @param token Token associated with account
     */
    public async loadMyAccountImage(account_id: string, token: string) {
        return this.http.put(
            constants.iroha_network + '/account/get/image',
            {
                username: account_id,
                token: token
            },
            {
                observe: 'response',
                responseType: 'blob' as 'json'
            }
        ).toPromise();
    }

    /**
     * This method sets some extra-data about an account, on the blockchain.
     * Supported options for info are:
     * - firstname: string
     * - surname: string
     * - phone: string
     * - email: string
     * - study: string (highest level of education)
     * - description: stringified Delta object
     * @param account_id The account fo which you need to set the extra-data
     * @param token The valid token associated with this account
     * @param info An object containing the data
     */
    public async setMyAccountInfo(account_id: string, token: string, info: any) {

        return this.http.post(
            constants.iroha_network + '/account/set/details',
            {
                account: account_id,
                token: token,
                info: info
            },
            {
                responseType: 'json',
                observe: 'response'
            }
        ).toPromise();
    }

    /**
     * This method sets a new profile image for a given account.
     * 
     * @param formData FormData containing the account, token and image
     */
    public async setMyAccountImage(formData: FormData) {
        return this.http.post(
            constants.iroha_network + '/account/set/image',
            formData,
            {
                observe: 'response',
                responseType: 'json'
            }
        ).toPromise();
    }

    /**
     * This method returns whether an account-token pair is valid.
     * 
     * @param account_id The account that you want to verify a token for
     * @param token The token associated with the account
     */
    public async verifyToken(account_id: string, token: string) {
        return this.http.post(
            constants.iroha_network + '/account/verify',
            {
                username: account_id,
                token: token
            },
            {
                observe: 'response',
                responseType: 'json'
            }
        ).toPromise();
    }

    /**
     * This method will return one of the AccountTypes
     * 
     * @see AccountType
     * 
     * @param account_id The account id you want to check
     * @param token The token associated with this account
     */
    public async getMyAccountType(account_id: string, token: string) {
        try {
            let response_orchestrator = await this.isOrchestrator(account_id, token);
            let response_company = await this.isCompany(account_id, token);
            console.log(response_orchestrator.body);
            console.log(response_company.body);
            if(response_orchestrator.body['msg'] == true) {
                return AccountType.orchestrator;
            }
            else if (response_company.body['msg'] == true) {
                return AccountType.company;
            }
            else {
                return AccountType.developer;
            }
        }
        catch(reason) {
            console.error(reason.error);
            let notifier = new NotifierService();
            notifier.notifyUIkit(reason.error.msg, NotificationStatus.danger);

            // Credentials are invalid and we delete them
            localStorage.removeItem(JSON.parse(localStorage.getItem('account')).id);
            localStorage.removeItem('account')

            return null;
        }
    }

    /**
     * This method tells you if an account is orchestrator or not.
     * 
     * @param account_id Account that needs to be verified
     * @param token The token of that account
     */
    private async isOrchestrator(account_id: string, token: string) {
        return this.http.post(
            constants.iroha_network + '/is_orchestrator',
            {
                account: account_id,
                token: token
            },
            {
                observe: 'response',
                responseType: 'json'
            }
        ).toPromise();
    }

    /**
     * Returns a promise which resolves into a message containing a True or False
     * answer.
     * 
     * @param account_id Account that needs to be checked if it has company privileges
     * @param token Token associated with this account
     */
    private async isCompany(account_id: string, token: string) {
        return this.http.post(
            constants.iroha_network + '/is_company',
            {
                account_id: account_id,
                token: token
            },
            {
                responseType: 'json',
                observe: 'response'
            }
        ).toPromise();
    }

}

export enum AccountType {
    developer = 'developer',
    orchestrator = 'orchestrator',
    company = 'company'
}