import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { constants } from '../constants';

@Injectable({providedIn: 'root'})
export class CompanyManager {
    constructor(
        private http: HttpClient
    ){}

    /**
     * Sets extra information about a company directly on the blockchain.
     * The info object accepts the following attributes:
     * 
     * - company_name : string
     * 
     * @param account_id The company to set info to
     * @param token The valid token associated with this company
     * @param info The information to be set
     */
    public async setCompanyInfo (account_id: string, token: string, info: any) {
        return this.http.post(
            constants.iroha_network + '/company/set_info',
            {
                account_id: account_id,
                token: token,
                company_name: info.company_name
            },
            {
                responseType: 'json',
                observe: 'response'
            }
        ).toPromise();
    }

    /**
     * Gets extra info from the blockchain about a company.
     * 
     * @param account_id The company for which you want to get the extra info from the blockchain
     * @param token The token that's associated with the company
     */
    public async getCompanyInfo (account_id: string, token: string) {
        return this.http.post(
            constants.iroha_network + '/company/get_info',
            {
                account_id: account_id,
                token: token
            },
            {
                responseType: 'json',
                observe: 'response'
            }
        ).toPromise();
    }

    /**
     * Gets a list of all the assets on the blockchain
     */
    public async getAssetList() {
        return this.http.get(
            constants.iroha_network + '/asset/get/list',
            {
                observe: 'response',
                responseType: 'json'
            }
        ).toPromise();
    }

    /**
     * Publishes a new job on the job market. The job object must have the following
     * members:
     * 
     * - name: string
     * - description: string version of a Quill Delta object
     * - city: string
     * - study: string
     * - type: string
     * - salary: number
     * - skills: string[]
     * - skill_levels: number[]
     * 
     * @param account_id The company publishing the job
     * @param private_key The private key of the company
     * @param job The job object in string form
     */
    public async publishJob(account_id: string, private_key: string, job: string) {
        return this.http.post(
            constants.iroha_network + '/company/publish_job',
            {
                account_id: account_id,
                private_key: private_key,
                job: job
            },
            {
                observe: 'response',
                responseType: 'json'
            }
        ).toPromise();
    }

    public async getJobList() {
        return this.http.get(
            constants.iroha_network + '/job/get/list',
            {
                observe: 'response',
                responseType: 'json'
            }
        ).toPromise();
    }

    /**
     * Rertieves the logo of a company without authorization from the company.
     * The user must prove that he's logged in by providing an [account_id -- token] pair.
     * 
     * @param account_id Account id of the user initiating the request
     * @param token The token of the account
     * @param company The company whose logo must be retrieved
     */
    public async getCompanyLogo(account_id: string, token: string, company: string) {
        return this.http.post(
            constants.iroha_network + '/company/get/image',
            {
                account_id: account_id,
                token: token,
                company: company
            },
            {
                observe: 'response',
                responseType: 'blob' as 'json'
            }
        ).toPromise();
    }

    /**
     * This method can be used to retrieve some public data about a company without authorization.
     * It can be used by any authenticated user
     * 
     * @param account_id The account making the request
     * @param token The token associated with this account
     * @param company We get info about this company
     */
    public async getCompanyPublicInfo(account_id: string, token: string, company: string) {
        return this.http.post(
            constants.iroha_network + '/company/get_public_info',
            {
                account_id: account_id,
                token: token,
                company_id: company
            },
            {
                responseType: 'json',
                observe: 'response'
            }
        ).toPromise();
    }

    /**
     * Retrieves a list of jobs that a company published. No auth/autz required.
     * 
     * @param company_id The company to search for. Full id: 'account@domain'
     */
    public async getCompanyJobs(company_id: string) {
        return this.http.put(
            constants.iroha_network + '/job/get/company_list',
            {
                company_id: company_id
            },
            {
                observe: 'response',
                responseType: 'json'
            }
        ).toPromise();
    }

    /**
     * Sends a request to the server to delete a job. Autz. is required.
     * This process will eliminate the job from MongoDB and will burn exactly
     * one job token from the orchestrator.
     * 
     * @param account_id The company that published the job
     * @param token The token associated with the company account
     * @param job_id The job id that needs to be deleted
     */
    public async deleteJob(account_id: string, token: string, job_id: string) {
        return this.http.post(
            constants.iroha_network + '/job/delete',
            {
                account_id: account_id,
                token: token,
                job_id: job_id
            },
            {
                observe: 'response',
                responseType: 'json'
            }
        ).toPromise();
    }

    /**
     * Loads a list of job applicants from the server. Request can be made only by companies
     * and authorization is required.
     * 
     * @param account_id Company initiating the request
     * @param token Token of company
     * @param job_id Job that has the applicants
     */
    public async loadApplicants(account_id: string, token: string, job_id: string) {
        return this.http.post(
            constants.iroha_network + '/job/get/applicants',
            {
                account_id: account_id,
                token: token,
                job_id: job_id
            },
            {
                observe: 'response',
                responseType: 'json'
            }
        ).toPromise();
    }

    public async loadApplicantInfo(company_id: string, token: string, dev_id: string) {
        return this.http.post(
            constants.iroha_network + '/company/get/applicant',
            {
                company_id: company_id,
                token: token,
                dev_id: dev_id
            },
            {
                responseType: 'json',
                observe: 'response'
            }
        ).toPromise();
    }

    public async loadApplicantPhoto(company_id: string, token: string, dev_id: string) {
        return this.http.post(
            constants.iroha_network + '/company/get/applicant_photo',
            {
                company_id: company_id,
                token: token,
                dev_id: dev_id
            },
            {
                responseType: 'blob' as 'json',
                observe: 'response'
            }
        ).toPromise();
    }

    public async loadApplicantJobs(company_id: string, token: string, dev_id: string) {
        return this.http.post(
            constants.iroha_network + '/company/get/applicant_jobs',
            {
                company_id: company_id,
                token: token,
                dev_id: dev_id
            },
            {
                responseType: 'json',
                observe: 'response'
            }
        ).toPromise();
    }

    /**
     * The hiring mechanism is as follows:
     * 1. A job token is transfered from the orchestrator to the developer designated as hired.
     * 2. The job is deleted from the mongo job market
     * 3. The job is deleted from the company 
     * 4. The developer now appears as part of the company
     * 
     * @param company_id The company that hires
     * @param token The token of the company account
     * @param dev_id The developer that gets hired
     * @param job_id The job for which he gets hired
     */
    public async hire(company_id: string, token: string, dev_id: string, job_id: string) {
        return this.http.post(
            constants.iroha_network + '/company/hire',
            {
                company_id: company_id,
                token: token,
                dev_id: dev_id,
                job_id: job_id
            },
            {
                observe: 'response',
                responseType: 'json'
            }
        ).toPromise();
    }

    public async loadEmployees(company_id: string, token: string) {
        return this.http.post(
            constants.iroha_network + '/company/get/employees',
            {
                company_id: company_id,
                token: token
            },
            {
                observe: 'response',
                responseType: 'json'
            }
        ).toPromise();
    }

    public async createTask(company_id: string, token: string, private_key: string, dev_id: string, task: any) {
        return this.http.post(
            constants.iroha_network + '/company/create_task',
            {
                company_id: company_id,
                token: token,
                private_key: private_key,
                dev_id: dev_id,
                task: task
            },
            {
                observe: 'response',
                responseType: 'json'
            }
        ).toPromise();
    }

    public async getApplicantWallet(company_id: string, token: string, dev_id: string) {
        return this.http.post(
            constants.iroha_network + '/company/get/applicant_wallet',
            {
                company_id: company_id,
                token: token,
                dev_id: dev_id
            },
            {
                observe: 'response',
                responseType: 'json'
            }
        ).toPromise();
    }
}
