import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { constants } from "../constants";

@Injectable({providedIn: 'root'})
export class DevManager {
    constructor(
        private http: HttpClient
    ){}

    /**
     * The method adds the account_id on the list of the applicants for the job.
     * Companies can then choose one developer and give them the job.
     * 
     * @param account_id The account applying for the job
     * @param token The token associated with this account
     * @param company_id The company that published the job
     * @param job_id The job id
     */
    public async applyForJob(account_id: string, token: string, company_id: string, job_id: string) {
        return this.http.post(
            constants.iroha_network + '/job/apply',
            {
                account_id: account_id,
                token: token,
                company_id: company_id,
                job_id: job_id
            },
            {
                observe: 'response',
                responseType: 'json'
            }
        ).toPromise();
    }

    /**
     * This method requests a list of tasks from the python server. The list is served from the
     * mongo database and each task contains a task id.
     * 
     * IF the developer has a task token on the blockchain that was minted by his current compayn he can use that
     * together with a task id to exchange the task for on-blockchain rewards.
     * 
     * @param account_id The account requesting its task list
     * @param token The token associated with this account
     */
    public async loadTaskList(account_id: string, token: string) {
        return this.http.post(
            constants.iroha_network + '/task/get/list',
            {
                account_id: account_id,
                token: token
            },
            {
                observe: 'response',
                responseType: 'json'
            }
        ).toPromise();
    }

    public async cashRewards(account_id: string, token: string, task_id: string) {
        return this.http.post(
            constants.iroha_network + '/task/mark_done',
            {
                account_id: account_id,
                token: token,
                task_id: task_id
            },
            {
                observe: 'response',
                responseType: 'json'
            }
        ).toPromise();
    }

    public async getWallet(account_id: string, token: string) {
        return this.http.post(
            constants.iroha_network + '/account/get/wallet',
            {
                account_id: account_id,
                token: token
            },
            {
                observe: 'response',
                responseType: 'json'
            }
        ).toPromise();
    }
}
