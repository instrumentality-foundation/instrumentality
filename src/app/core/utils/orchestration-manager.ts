import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { NotifierService, NotificationStatus } from '../services/notifier.service';
import { constants } from '../constants';

@Injectable({providedIn: 'root'})
export class OrchestrationManager {

    constructor(
        private http: HttpClient,
        private notifier: NotifierService
    ){}

    private domain_pattern = /^[A-Za-z_0-9]{1,64}$/
    private asset_pattern = /^[a-z_0-9]{1,32}$/

    /**
     * This method makes a request to the server asking that a new company
     * be added on the Iroha blockchain.
     * 
     * @param account_id Id of account requesting the creation of a new company
     * @param token The valid token of the account
     * @param companyName The name of the new company
     */
    public async registerCompany(account_id: string, token: string, companyName: string) {
        // Testing company name format against pattern
        if(!this.domain_pattern.test(companyName)) {
            console.error('Wrong company name format!');
            this.notifier.notifyUIkit("Use only letters, numbers and '_'", NotificationStatus.danger);
            return null;
        }

        return this.http.post(
            constants.iroha_network + '/company/create',
            {
                account: account_id,
                token: token,
                companyName: companyName
            },
            {
                observe: 'response',
                responseType: 'json'
            }
        ).toPromise();
    }

    /**
     * This method request that a new asset be created on the Iroha
     * blockchain in the 'instrumentality' domain.
     * 
     * @param account_id The account creating the request
     * @param token The token associated with this account
     * @param asset_name The name of the asset to be created
     */
    public async createAsset(account_id: string, token: string, assetName: string) {
        if (!this.asset_pattern.test(assetName)) {
            console.error('Wrong asset name format!');
            this.notifier.notifyUIkit("Use only small letters, numbers and '_'", NotificationStatus.danger);
            return null;
        }

        return this.http.post(
            constants.iroha_network + '/asset/create',
            {
                account: account_id,
                token: token,
                asset_name: assetName,
                precision: 6
            },
            {
                observe: 'response',
                responseType: 'json'
            }
        ).toPromise();
    }

    /**
     * This method requests the server to add a new ammount of an asset to existing reserves of 
     * this account.
     * 
     * @param account_id The account requesting the minting of a new amount from a specific asset
     * @param token The token of the account
     * @param assetName The name of the asset
     * @param assetAmount The quantity of asset you want to mint
     */
    public async mintAsset(account_id: string, token: string, assetName: string, assetAmount: number) {
        if(assetAmount < 0) {
            this.notifier.notifyUIkit('Amount must be greater than 0', NotificationStatus.danger);
            return null;
        }

        return this.http.post (
            constants.iroha_network + '/asset/mint',
            {
                account: account_id,
                token: token,
                asset_name: assetName,
                amount: assetAmount
            },
            {
                observe: 'response',
                responseType: 'json'
            }
        ).toPromise();
    }

}