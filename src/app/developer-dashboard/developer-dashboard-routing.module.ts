import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { DeveloperDashboardComponent } from "./developer-dashboard.component";
import { ProfileComponent } from "./profile/profile.component";
import { JobsComponent } from './jobs/jobs.component';
import { WalletComponent } from './wallet/wallet.component';

const routes: Routes = [
  {
    path: '',
    component: DeveloperDashboardComponent,
    children: [
      {path: 'profile', component: ProfileComponent},
      {path: 'jobs', component: JobsComponent},
      {path: 'wallet', component: WalletComponent},
      {path: '', redirectTo: 'profile', pathMatch: 'full'}
    ]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class DeveloperDashboardRoutingModule { }
