import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FontAwesomeModule } from "@fortawesome/angular-fontawesome";
import { FormsModule } from "@angular/forms";

import { DeveloperDashboardRoutingModule } from './developer-dashboard-routing.module';
import { DeveloperDashboardComponent } from './developer-dashboard.component';
import { NavbarComponent } from './navbar/navbar.component';
import { ProfileComponent } from './profile/profile.component';
import { CoreModule } from '../core/core.module';
import { JobsComponent } from './jobs/jobs.component';
import { WalletComponent } from './wallet/wallet.component';
import { WalletPipe } from './wallet.pipe';


@NgModule({
  declarations: [DeveloperDashboardComponent, NavbarComponent, ProfileComponent, JobsComponent, WalletComponent, WalletPipe],
  imports: [
    CommonModule,
    DeveloperDashboardRoutingModule,
    FontAwesomeModule,
    FormsModule,
    CoreModule
  ]
})
export class DeveloperDashboardModule { }
