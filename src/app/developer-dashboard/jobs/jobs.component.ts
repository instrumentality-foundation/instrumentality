import { Component, OnInit } from '@angular/core';
import { CompanyManager } from 'src/app/core/utils/company-manager';

@Component({
  selector: 'developer-jobs',
  templateUrl: './jobs.component.html',
  styleUrls: ['./jobs.component.less']
})
export class JobsComponent implements OnInit {

  public jobs = [];

  constructor(
    private companyManager: CompanyManager
  ) { }

  ngOnInit(): void {
    this.companyManager.getJobList()
    .then(response => {
      this.jobs = response.body['jobs'];
    })
    .catch(reason => {
      console.error(reason);
    });
  }

}
