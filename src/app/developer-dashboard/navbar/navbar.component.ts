import { Component, OnInit } from '@angular/core';
import { faSignOutAlt, faBars } from "@fortawesome/free-solid-svg-icons";

@Component({
  selector: 'developer-navbar',
  templateUrl: './navbar.component.html',
  styleUrls: [
    './navbar.component.less',
    '../../core/styles/navbar.less'
  ]
})
export class NavbarComponent implements OnInit {

  // Icon definitions
  public faSignOutAlt = faSignOutAlt;
  public faBars = faBars;

  constructor() { }

  ngOnInit(): void {
  }

}
