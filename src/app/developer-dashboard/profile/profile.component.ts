import { Component, OnInit, AfterViewInit, ViewChild, ElementRef } from '@angular/core';
import { faPencilAlt, faCameraRetro } from "@fortawesome/free-solid-svg-icons";
import { NotifierService, NotificationStatus } from 'src/app/core/services/notifier.service';
import { AccountManager } from 'src/app/core/utils/account-manager';
import { SpinnerCombo } from 'src/app/core/services/spinner.service';
import * as Quill from 'quill';
import { DevManager } from 'src/app/core/utils/dev-manager';

@Component({
  selector: 'developer-profile',
  templateUrl: './profile.component.html',
  styleUrls: [
    './profile.component.less',
    '../../core/styles/profile-section.less'
  ]
})
export class ProfileComponent implements OnInit {

  // Icons
  public faPencilAlt = faPencilAlt;
  public faCameraRetro = faCameraRetro;

  // Button & spinner
  @ViewChild('save_button', {static: true}) saveButton: ElementRef;
  @ViewChild('save_spinner', {static: true}) saveSpinner: ElementRef;

  // Modal elements
  @ViewChild('modal_profile', {static: true}) modalProfile: ElementRef;
  @ViewChild('profile_image', {static: true}) profileImage: ElementRef;

  // Account credentials
  public account_id: string = JSON.parse(localStorage.getItem('account')).id;
  private token: string = JSON.parse(localStorage.getItem('account')).token;

  // Form input fields bindings
  public edit_fname: any;
  public edit_sname: any;
  public edit_phone: string;
  public edit_email: string;
  public edit_study: string;
  public edit_city: string;
  public editor: any;

  // Others
  public tasks = [];
  public task_quill: Quill;

  constructor(
    private notifier: NotifierService,
    private accountManager: AccountManager,
    private devManager: DevManager
  ) { }

  ngOnInit(): void {
    // Load quill
    this.editor = new Quill('#quill_editor',
    {
      theme: 'snow',
      placeholder: 'Describe yourself'
    });

    // We load the extra data account from memory
    let data = localStorage.getItem(this.account_id) != null ? JSON.parse(localStorage.getItem(this.account_id)) : null;
    if (data) {
      this.edit_fname = data.firstname;
      this.edit_sname = data.surname;
      this.edit_email = data.email;
      this.edit_city = data.city;
      this.edit_phone = data.phone;
      this.edit_study = data.study;
      this.editor.setContents(JSON.parse(data.description));
    }
    else {
      // We request the data from blockchain
      let request = this.accountManager.loadMyAccountData(this.account_id, this.token);
      request.then(response => {
        console.log(response.body);
        localStorage.setItem(this.account_id, JSON.stringify({
          firstname: response.body['firstname'], 
          surname: response.body['surname'],
          phone: response.body['phone'],
          email: response.body['email'],
          city: response.body['city'],
          study: response.body['study'],
          description: response.body['description']
        }));
        this.edit_fname = response.body['firstname'];
        this.edit_sname = response.body['surname'];
        this.edit_email = response.body['email'];
        this.edit_city = response.body['city'];
        this.edit_phone = response.body['phone'];
        this.edit_study = response.body['study'];
        this.editor.setContents(JSON.parse(response.body['description']));
      })
      .catch(reason => {
        console.error(reason.error);
      })
    }

    // Loading tasks from the server
    this.devManager.loadTaskList(this.account_id, this.token)
    .then(response => {
      console.log("Successfully retrieved tasks");
      this.tasks = response.body['tasks'];
    })
    .catch(reason => {
      console.error(reason.error);
      this.notifier.notifyUIkit(reason.error.msg, NotificationStatus.danger);
    });
  }

  ngAfterViewInit(): void {
    // Load the profile image
    let request = this.accountManager.loadMyAccountImage(this.account_id, this.token);
    request.then(response => {
      console.log(response.body);
      this.modalProfile.nativeElement.src = URL.createObjectURL(response.body);
      this.profileImage.nativeElement.src = URL.createObjectURL(response.body);
    })
    .catch(reason => {
      console.error(reason.error);
    });

    this.task_quill = new Quill('#task_quill', {
      module: {
        toolbar: false
      },
      theme: 'snow',
      readOnly: true
    });
  }

  public async setInfo() {
    // Get the information from the modal form
    let firstname = this.edit_fname != null ? this.edit_fname : '';
    let surname = this.edit_sname != null ? this.edit_sname : '';
    let info = {
      firstname: firstname,
      surname: surname,
      phone: this.edit_phone,
      email: this.edit_email,
      city: this.edit_city,
      study: this.edit_study,
      description: JSON.stringify(this.editor.getContents()) 
    }

    // Create a spinner combo
    let spinnerCombo = new SpinnerCombo(this.saveButton, this.saveSpinner)
    spinnerCombo.toggle();

    //Making the request to the blockchain to save the data
    try {
      let response = await this.accountManager.setMyAccountInfo(this.account_id, this.token, info);
      console.log(response.body['msg']);
      this.notifier.notifyUIkit(response.body['msg'], NotificationStatus.sucess);
      localStorage.setItem(this.account_id,
        JSON.stringify(info));
    }
    catch(reason) {
      console.error(reason);
      this.notifier.notifyUIkit(reason.error.msg, NotificationStatus.danger);
    }
    finally {
      spinnerCombo.toggle();
    }
  }

  public async changeProfilePicture() {
    // Create file input on-the-fly
    let input = document.createElement('input');
    input.type = 'file';
    input.accept = "image/svg, image/png, image/jpg, image/jpeg"
    input.click();

    let form_data = new FormData();

    let file : File;
    
    // At input onchange event we make the server request
    input.onchange = async event => {
      let target = event.target as any;
      file = target.files[0];

      form_data.append('image', file);
      form_data.append('username', this.account_id);
      form_data.append('token', this.token);

      try {
        let response = await this.accountManager.setMyAccountImage(form_data);
        console.log(response.body);
        this.notifier.notifyUIkit('Profile picture set successfully', NotificationStatus.sucess);
      
        // We now load the picture
        try {
          let response2 = await this.accountManager.loadMyAccountImage(this.account_id, this.token);
          console.log(response2.body);
          this.modalProfile.nativeElement.src = URL.createObjectURL(response2.body);
          this.profileImage.nativeElement.src = URL.createObjectURL(response2.body);
        }
        catch (reason) {
          console.error(reason.error);
        }
      }
      catch(reason) {
        console.error(reason.error);
        this.notifier.notifyUIkit(reason.error, NotificationStatus.danger);
      }
    }
  }

}
