import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'walletFilter'
})
export class WalletPipe implements PipeTransform {

  transform(items: Skill[], searchText: string): Skill[] {
    if (!items) {
      return [];
    }
    if (!searchText) {
      return items;
    }

    searchText = searchText.toLowerCase();

    return items.filter(item => {
      return item.name.includes(searchText);
    });
  }

}

class Skill {
  name: string;
  balance: string;

  constructor(name: string, balance: string) {
    this.name = name;
    this.balance = balance;
  }
}
