import { Component, OnInit, PipeTransform, Pipe } from '@angular/core';
import { DevManager } from 'src/app/core/utils/dev-manager';
import { NotifierService, NotificationStatus } from 'src/app/core/services/notifier.service';

@Component({
  selector: 'developer-wallet',
  templateUrl: './wallet.component.html',
  styleUrls: ['./wallet.component.less']
})
export class WalletComponent implements OnInit {

  public wallet: Skill[] = [];
  public searchText: string;

  account_id = JSON.parse(localStorage.getItem('account')).id;
  token = JSON.parse(localStorage.getItem('account')).token;

  constructor(
    private devManager: DevManager,
    private notifier: NotifierService
  ) { }

  ngOnInit(): void {
    this.devManager.getWallet(this.account_id, this.token)
    .then(response => {
      response.body['assets'].forEach(asset => {
        if (!(asset.asset_id.split('#')[0] == 'job')){
          let skill = new Skill(asset.asset_id, asset.balance)
          this.wallet.push(skill)
        }
      });
    })
    .catch(reason => {
      console.error(reason.error);
      this.notifier.notifyUIkit(reason.error.msg, NotificationStatus.danger);
    });
  }

}

class Skill {
  name: string;
  balance: string;

  constructor(name: string, balance: string) {
    this.name = name.split('#')[0];
    this.balance = balance;
  }
}
