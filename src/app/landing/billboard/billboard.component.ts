import { Component, OnInit, Input } from '@angular/core';

@Component({
  selector: 'landing-billboard',
  templateUrl: './billboard.component.html',
  styleUrls: ['./billboard.component.less']
})
export class BillboardComponent implements OnInit {

  @Input() public title: string;
  @Input() public catchphrase: string;

  constructor() { }

  ngOnInit(): void {
  }

}
