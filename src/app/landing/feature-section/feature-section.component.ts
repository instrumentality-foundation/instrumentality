import { Component, OnInit, Input } from '@angular/core';

@Component({
  selector: 'landing-feature-section',
  templateUrl: './feature-section.component.html',
  styleUrls: ['./feature-section.component.less']
})
export class FeatureSectionComponent implements OnInit {

  @Input() public image: string;
  @Input() public description: string;
  @Input() public title: string;

  constructor() { }

  ngOnInit(): void {
  }

}
