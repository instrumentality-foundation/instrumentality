import { Component, OnInit, Input } from '@angular/core';

@Component({
  selector: 'landing-infopanel',
  templateUrl: './infopanel.component.html',
  styleUrls: ['./infopanel.component.less']
})
export class InfopanelComponent implements OnInit {

  @Input() public image: string;
  @Input() public description: string;
  @Input() public title: string;
  @Input() public catchphrase: string;

  constructor() { }

  ngOnInit(): void {
  }

}
