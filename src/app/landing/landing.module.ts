import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FontAwesomeModule } from "@fortawesome/angular-fontawesome";
import { CoreModule } from "../core/core.module";
import { FormsModule } from "@angular/forms";

import { LandingRoutingModule } from './landing-routing.module';
import { LandingComponent } from './landing.component';
import { NavbarComponent } from './navbar/navbar.component';
import { HomeComponent } from './home/home.component';
import { BillboardComponent } from './billboard/billboard.component';
import { FeatureSectionComponent } from './feature-section/feature-section.component';
import { AboutComponent } from './about/about.component';
import { InfopanelComponent } from './infopanel/infopanel.component';

@NgModule({
  declarations: [LandingComponent, NavbarComponent, HomeComponent, BillboardComponent, FeatureSectionComponent, AboutComponent, InfopanelComponent],
  imports: [
    CommonModule,
    LandingRoutingModule,
    FontAwesomeModule,
    CoreModule,
    FormsModule
  ]
})
export class LandingModule { }
