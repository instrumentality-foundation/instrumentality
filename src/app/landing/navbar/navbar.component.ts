import { Component, OnInit, ViewChild, ElementRef } from '@angular/core';
import { faBars } from "@fortawesome/free-solid-svg-icons";
import { AuthenticationService } from 'src/app/core/services/authentication.service';
import { NotifierService, NotificationStatus } from 'src/app/core/services/notifier.service';
import * as UIkit from "uikit";
import { SpinnerCombo } from 'src/app/core/services/spinner.service';
import { Router } from '@angular/router';
import { AccountManager, AccountType } from 'src/app/core/utils/account-manager';

@Component({
  selector: 'landing-navbar',
  templateUrl: './navbar.component.html',
  styleUrls: [
    './navbar.component.less',
    '../../core/styles/navbar.less'
  ]
})
export class NavbarComponent implements OnInit {

  // Icon declaration
  public faBars = faBars;

  // Form inputs
  public login_user: any;
  public login_key: any;
  public signup_user: any;

  // Buttons & Spinners
  @ViewChild('login_button', {static: true}) loginButton: ElementRef;
  @ViewChild('signup_button', {static: true}) signupButton: ElementRef;
  @ViewChild('login_spinner', {static: true}) loginSpinner: ElementRef;
  @ViewChild('signup_spinner', {static: true}) signupSpinner: ElementRef;

  constructor(
    private auth: AuthenticationService,
    private notifier: NotifierService,
    private router: Router,
    private accountManager: AccountManager
  ) { }

  ngOnInit(): void {
  }

  private showKeys(privateKey: string, account_id: string) {
    let html =
    `
    <div class="uk-modal-header uk-text-center" style="background-color: #1B2021;">
        <h2 style="color: #FFEAEE; font-family: 'Lato', sans-serif;">Please save this key in a safe place and take note of your username!</h2>
    </div>
    <div class="uk-modal-body uk-flex uk-flex-column uk-flex-middle uk-flex-center uk-text-center" style="background-color: #1B2021;">
        <h3 style="color: #FFEAEE; font-family: 'Lato', sans-serif;">HI,</h3>
        <h3 style="word-break: break-all; font-weight: 700; font-size: 2em; color: #F7BFBE; font-family: 'Lato', sans-serif;">${account_id}</h3>
        <h3 style="color: #FFEAEE; font-family: 'Lato', sans-serif;">Your private key is:</h3>
        <p style="word-break: break-all; color: #F7BFBE; font-family: 'Lato', sans-serif;">${privateKey}</p>
    </div>
    <div class="uk-modal-footer uk-flex uk-flex-middle uk-flex-center" style="background-color: #1B2021;">
        <button style="font-family: 'Lato', sans-serif; font-weight: 700; font-size: 1.2em; background-color: #F7BFBE; color: #1B2021; padding: .3em; border-radius: .2em;" class="uk-button uk-button-primary uk-modal-close" type="button">I saved the key</button>
    </div>
    `;
    this.notifier.createModalUIkit(html);
  }

  public async login() {
    // Create spinner
    let spinnerCombo = new SpinnerCombo(this.loginButton, this.loginSpinner);
    spinnerCombo.toggle();

    try {
      let response = await this.auth.authenticateAccount(this.login_user, this.login_key);
      console.log(response.body);
      this.notifier.notifyUIkit(response.body['msg'], NotificationStatus.sucess);

      // Save temporary credentials in local storage
      localStorage.setItem(
        'account',
        JSON.stringify({
          id: this.login_user,
          token: response.body['token']
        })
      )

      // Navigate to respective dashboards
      if (await this.accountManager.getMyAccountType(this.login_user, response.body['token']) == AccountType.orchestrator) {
        this.router.navigateByUrl('/orchestrator/deck');
      }
      else if (await this.accountManager.getMyAccountType(this.login_user, response.body['token']) == AccountType.company) {
        this.router.navigateByUrl('/company');
      }
      else {
        this.router.navigateByUrl('/account/' + this.login_user);
      }
    }
    catch(reason) {
      console.error(reason);
      this.notifier.notifyUIkit(reason.error.msg, NotificationStatus.danger);
    }
    finally {
      spinnerCombo.toggle();
    }
  }

  public async register(event: Event) {
    // Get parent modal
    let el = event.target as HTMLElement;
    let modal = el.parentElement.parentElement.parentElement.parentElement;

    // Create spinner
    let spinnerCombo = new SpinnerCombo(this.signupButton, this.signupSpinner);
    spinnerCombo.toggle();

    try {
      let response = await this.auth.registerAccount(this.signup_user);
      console.log(response.body);
      this.notifier.notifyUIkit("Account created successfully", NotificationStatus.sucess);
      UIkit.modal(modal).hide();
      this.showKeys(response.body['private_key'], this.signup_user + "@instrumentality");
    }
    catch (reason) {
      console.error(reason);
      this.notifier.notifyUIkit(reason.error.msg, NotificationStatus.danger);
    }
    finally {
      spinnerCombo.toggle();
    }
  }

}
