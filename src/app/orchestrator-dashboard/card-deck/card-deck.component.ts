import { Component, OnInit, ViewChild, ElementRef } from '@angular/core';
import { NotifierService, NotificationStatus } from 'src/app/core/services/notifier.service';
import { OrchestrationManager } from 'src/app/core/utils/orchestration-manager';
import { SpinnerCombo } from 'src/app/core/services/spinner.service';

@Component({
  selector: 'orchestrator-card-deck',
  templateUrl: './card-deck.component.html',
  styleUrls: ['./card-deck.component.less']
})
export class CardDeckComponent implements OnInit {

   //////////////////////////////
  // Register Company command //
 //////////////////////////////
  public companyName: any;
  @ViewChild('company_button', {static: true}) public companyButton: ElementRef;
  @ViewChild('company_spinner', {static: true}) public companySpinner: ElementRef;

  private showCompanyKey(companyName: string, privateKey: string) {
    let html = 
    `
    <div class="uk-modal-header uk-text-center" style="background-color: #1B2021;">
        <h2 style="color: #FFEAEE; font-family: 'Lato', sans-serif;">Please give this key to the company contact!</h2>
    </div>
    <div class="uk-modal-body uk-flex uk-flex-column uk-flex-middle uk-flex-center uk-text-center" style="background-color: #1B2021;">
        <h3 style="word-break: break-all; font-weight: 700; font-size: 2em; color: #F7BFBE; font-family: 'Lato', sans-serif;">${companyName.toLowerCase()}@${companyName}</h3>
        <h3 style="color: #FFEAEE; font-family: 'Lato', sans-serif;">The private key for this company is:</h3>
        <p style="word-break: break-all; color: #F7BFBE; font-family: 'Lato', sans-serif;">${privateKey}</p>
    </div>
    <div class="uk-modal-footer uk-flex uk-flex-middle uk-flex-center" style="background-color: #1B2021;">
        <button style="font-family: 'Lato', sans-serif; font-weight: 700; font-size: 1.2em; background-color: #F7BFBE; color: #1B2021; padding: .3em; border-radius: .2em;" class="uk-button uk-button-primary uk-modal-close" type="button">I passed the key</button>
    </div>
    `;

    this.notifier.createModalUIkit(html);
  }

  public async registerCompany() {
    // Create spinner
    let spinnerCombo = new SpinnerCombo(this.companyButton, this.companySpinner);
    spinnerCombo.toggle();

    try {
      let response = await this.orchestrationManager.registerCompany(this.account.id, this.account.token, this.companyName);
      if (response != null){
        console.log(response.body);
        this.notifier.notifyUIkit(response.body['msg'], NotificationStatus.sucess);
        this.showCompanyKey(this.companyName, response.body['private_key']);
      }
    }
    catch(reason) {
      console.error(reason.error);
      this.notifier.notifyUIkit(reason.error.msg, NotificationStatus.danger);
    }
    finally {
      spinnerCombo.toggle();
    }
  }

   ////////////////////////////
  // Register Asset command //
 ////////////////////////////
  public assetName: any;
  @ViewChild('new_asset_button', {static: true}) public newAssetButton: ElementRef;
  @ViewChild('new_asset_spinner', {static: true}) public newAssetSpinner: ElementRef;

  public async createAsset(){
    // Create spinner combo
    let spinnerCombo = new SpinnerCombo(this.newAssetButton, this.newAssetSpinner);
    spinnerCombo.toggle();

    try {
      let response = await this.orchestrationManager.createAsset(this.account.id, this.account.token, this.assetName);
      if (response) {
        console.log(response.body);
        this.notifier.notifyUIkit(response.body['msg'], NotificationStatus.sucess);
      }
    }
    catch(reason) {
      console.error(reason.error);
      this.notifier.notifyUIkit(reason.error.msg, NotificationStatus.danger);
    }
    finally {
      spinnerCombo.toggle();
    }
  }

   ////////////////////////
  // Mint Asset command //
 ////////////////////////
  public mint_asset_name: any;
  public mint_asset_amount: any;
  @ViewChild('mint_asset_button', {static: true}) public mintAssetButton: ElementRef;
  @ViewChild('mint_asset_spinner', {static: true}) public mintAssetSpinner: ElementRef;

  public async mintAsset() {
    // Create spinner combo
    let spinnerCombo = new SpinnerCombo(this.mintAssetButton, this.mintAssetSpinner);
    spinnerCombo.toggle();

    try {
      let response = await this.orchestrationManager.mintAsset(
        this.account.id,
        this.account.token,
        this.mint_asset_name,
        this.mint_asset_amount
      );
      if (response) {
        console.log(response.body);
        this.notifier.notifyUIkit(response.body['msg'], NotificationStatus.sucess);
      }
    }
    catch(reason) {
      console.error(reason.error);
      this.notifier.notifyUIkit(reason.error.msg, NotificationStatus.danger);
    }
    finally {
      spinnerCombo.toggle();
    }
  }


  private account = JSON.parse(localStorage.getItem('account'));

  constructor(
    private notifier: NotifierService,
    private orchestrationManager: OrchestrationManager 
  ) { }

  ngOnInit(): void {
  }

}
