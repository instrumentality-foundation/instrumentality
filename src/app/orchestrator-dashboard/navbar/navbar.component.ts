import { Component, OnInit } from '@angular/core';
import { faSignOutAlt } from "@fortawesome/free-solid-svg-icons";

@Component({
  selector: 'orchestrator-navbar',
  templateUrl: './navbar.component.html',
  styleUrls: [
    './navbar.component.less',
    '../../core/styles/navbar.less'
  ]
})
export class NavbarComponent implements OnInit {

  // Icons
  public faSignOutAlt = faSignOutAlt;

  constructor() { }

  ngOnInit(): void {
  }

}
