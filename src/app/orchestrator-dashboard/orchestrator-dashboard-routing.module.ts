import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { OrchestratorDashboardComponent } from "./orchestrator-dashboard.component";
import { CardDeckComponent } from "./card-deck/card-deck.component";

const routes: Routes = [
  {
    path: '',
    component: OrchestratorDashboardComponent,
    children: [
      {path: 'deck', component: CardDeckComponent},
      {path: '', redirectTo: 'deck', pathMatch: 'full'}
    ]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class OrchestratorDashboardRoutingModule { }
