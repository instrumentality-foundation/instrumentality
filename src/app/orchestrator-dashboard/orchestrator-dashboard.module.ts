import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { CoreModule } from "../core/core.module";
import { FontAwesomeModule } from "@fortawesome/angular-fontawesome";
import { FormsModule } from "@angular/forms";

import { OrchestratorDashboardRoutingModule } from './orchestrator-dashboard-routing.module';
import { OrchestratorDashboardComponent } from './orchestrator-dashboard.component';
import { CardDeckComponent } from './card-deck/card-deck.component';
import { NavbarComponent } from './navbar/navbar.component';


@NgModule({
  declarations: [OrchestratorDashboardComponent, CardDeckComponent, NavbarComponent],
  imports: [
    CommonModule,
    OrchestratorDashboardRoutingModule,
    CoreModule,
    FontAwesomeModule,
    FormsModule
  ]
})
export class OrchestratorDashboardModule { }
