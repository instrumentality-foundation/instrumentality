import { Component, OnInit, AfterViewInit, ViewChild, ElementRef } from '@angular/core';
import { ActivatedRoute } from "@angular/router";
import { CompanyManager } from 'src/app/core/utils/company-manager';
import { NotifierService, NotificationStatus } from 'src/app/core/services/notifier.service';
import * as Quill from 'quill';
import { faMapMarkerAlt, faEnvelope, faGraduationCap, faMobile } from "@fortawesome/free-solid-svg-icons";
import * as randomcolor from 'randomcolor';

@Component({
  selector: 'static-profile',
  templateUrl: './profile.component.html',
  styleUrls: ['./profile.component.less']
})
export class ProfileComponent implements OnInit {

  public developer_id: string;
  public dev: any;
  public quill_editor: Quill;
  public jobs = [];
  public wallet: Skill[] = [];
  public skill_colors = [];

  //Icons
  public faMapMarkerAlt = faMapMarkerAlt;
  public faEnvelope = faEnvelope;
  public faGraduationCap = faGraduationCap;
  public faMobile = faMobile;

  // DOM elements
  @ViewChild('dev_photo', {static: true}) public dev_photo: ElementRef;
  @ViewChild('hire_spinner', {static: true}) public hire_spinner: ElementRef;

  // Credentials
  public company_id: string = JSON.parse(localStorage.getItem('account')).id;
  private token: string = JSON.parse(localStorage.getItem('account')).token;

  constructor(
    private route: ActivatedRoute,
    private companyManager: CompanyManager,
    private notifier: NotifierService
  ) { }

  ngOnInit(): void {
    this.developer_id = this.route.parent.snapshot.params.account;

    // Get the developer's account info
    this.companyManager.loadApplicantInfo(this.company_id, this.token, this.developer_id)
    .then(response => {
      this.dev = response.body;
    })
    .catch(reason => {
      console.error(reason.error);
      this.notifier.notifyUIkit(reason.error.msg, NotificationStatus.danger);
    });

    // Load photo
    this.companyManager.loadApplicantPhoto(this.company_id, this.token, this.developer_id)
    .then(response => {
      this.dev_photo.nativeElement.src = URL.createObjectURL(response.body);
    })
    .catch(reason => {
      console.log(reason.error);
    });

    // Load jobs
    this.companyManager.loadApplicantJobs(this.company_id, this.token, this.developer_id)
    .then(response => {
      console.log(response.body['jobs'])
      this.jobs = response.body['jobs'];
    })
    .catch(reason => {
      console.error(reason.error);
    });

    // Load description
    this.quill_editor = new Quill('#quill_editor',{
      modules: {
        toolbar: false
      },
      theme: 'snow',
      readOnly: true
    });
    
    // Load wallet
    this.companyManager.getApplicantWallet(this.company_id, this.token, this.developer_id)
    .then(response => {
      response.body['assets'].forEach(asset => {
        if (!(asset.asset_id.split('#')[0] == 'job')){
          let skill = new Skill(asset.asset_id, asset.balance)
          this.wallet.push(skill)
        }
      })
      // Sorting wallet
      this.wallet = this.wallet.sort((a: Skill, b: Skill) => {
        let a_value = parseFloat(a.balance)
        let b_value = parseFloat(b.balance)

        return a_value - b_value
      });
      
      this.wallet = this.wallet.slice(0,5)

      this.skill_colors = randomcolor.randomColor({count: this.wallet.length, luminosity: 'light'})
    })
    .catch(reason => {
      console.error(reason.error);
      this.notifier.notifyUIkit(reason.error.msg, NotificationStatus.danger);
    });

  }

  ngAfterViewInit(): void {
    this.quill_editor.setContents (JSON.parse(this.dev.description));
  }

  public async hire(job_id: string) {
    this.hire_spinner.nativeElement.style.display = 'block';
    try{
      let response = await this.companyManager.hire(this.company_id, this.token, this.developer_id, job_id);
      console.log(response.body);
      this.notifier.notifyUIkit(response.body['msg'], NotificationStatus.sucess);
    }
    catch(reason) {
      console.error(reason.error);
      this.notifier.notifyUIkit(reason.error.msg, NotificationStatus.danger);
    }
    finally {
      this.hire_spinner.nativeElement.style.display = 'none';
    }
  }

}

class Skill {
  name: string;
  balance: string;

  constructor(name: string, balance: string) {
    this.name = name.split('#')[0];
    this.balance = balance;
  }
}
