import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FontAwesomeModule } from "@fortawesome/angular-fontawesome";

import { StaticRoutingModule } from './static-routing.module';
import { StaticComponent } from './static.component';
import { ProfileComponent } from './profile/profile.component';


@NgModule({
  declarations: [StaticComponent, ProfileComponent],
  imports: [
    CommonModule,
    StaticRoutingModule,
    FontAwesomeModule
  ]
})
export class StaticModule { }
